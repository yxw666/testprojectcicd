module zaptest

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.10.0 // indirect
	github.com/google/go-containerregistry v0.8.0
	github.com/jasonlvhit/gocron v0.0.1
	github.com/robfig/cron v1.2.0
	github.com/ugorji/go v1.2.6 // indirect
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
)
