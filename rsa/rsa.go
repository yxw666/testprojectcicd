package rsa

import (
	"crypto"
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

func GetTextFromPemFile(path string) []byte {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("read file fail", err)
		return []byte("")
	}
	defer f.Close()

	fd, err := ioutil.ReadAll(f)
	if err != nil {
		fmt.Println("read to fd fail", err)
		return []byte("")
	}
	return fd
}

func getMd5String1(str string) string {
	m := md5.New()
	_, err := io.WriteString(m, str)
	if err != nil {
		log.Fatal(err)
	}
	arr := m.Sum(nil)
	return fmt.Sprintf("%x", arr)
}

func maintest() {
	var data = "apusic$2021"
	md5string := getMd5String1(data)
	fmt.Println(md5string)

	//rsa 密钥文件产生
	fmt.Println("-------------------------------获取RSA公私钥-----------------------------------------")
	//prvKey, pubKey := GenRsaKey()
	prvKey := GetTextFromPemFile("private.pem")
	pubKey := GetTextFromPemFile("public.pem")
	fmt.Println(string(prvKey))
	fmt.Println(string(pubKey))

	fmt.Println("-------------------------------进行签名与验证操作-----------------------------------------")
	//var data = "卧了个槽，这么神奇的吗？？！！！  ԅ(¯﹃¯ԅ) ！！！！！！）"

	fmt.Println("对消息进行签名操作...")
	//签名数据 signData
	signData := RsaSignWithSha256([]byte(data), prvKey)
	fmt.Println("消息的签名信息： ", hex.EncodeToString(signData))
	fmt.Println("\n对签名信息进行验证...")
	if RsaVerySignWithSha256([]byte(data), signData, pubKey) {
		fmt.Println("签名信息验证成功，确定是正确私钥签名！！")
	}

	fmt.Println("-------------------------------进行加密解密操作-----------------------------------------")
	ciphertext := RsaEncrypt([]byte(data), pubKey)
	fmt.Println("公钥加密后的数据：", hex.EncodeToString(ciphertext))
	sourceData := RsaDecrypt(ciphertext, prvKey)
	fmt.Println("私钥解密后的数据：", string(sourceData))
}

/**
//RSA公钥私钥产生

//私钥
1.使用rsa中的GenerateKey方法生成私钥
func GenerateKey(random io.Reader, bits int) (priv *PrivateKey, err error)
rand.Reader ->随机数生成器
bits ->建议1024的整数倍



2.通过x509标准将得到的ras私钥序列化为ASN.1 的 DER编码字符串
func MarshalPKCS1PrivateKey(key *rsa.PrivateKey) []byte


3.将私钥字符串设置到pem格式块中
初始化一个pem.Block块
type Block struct {
   Type    string            // 得自前言的类型（如"RSA PRIVATE KEY"）
   Headers map[string]string // 可选的头项
   Bytes   []byte            // 内容解码后的数据，一般是DER编码的ASN.1结构
}


4.通过pem将设置好的数据进行编码, 并写入磁盘文件中
func Encode(out io.Writer, b *Block) error
out - 准备一个文件指针
block- 将准备好的pem.block放入其中



 //公钥
1.从得到的私钥对象中将公钥信息取出
type PrivateKey struct {
    PublicKey            // 公钥
    D         *big.Int   // 私有的指数
    Primes    []*big.Int // N的素因子，至少有两个
    // 包含预先计算好的值，可在某些情况下加速私钥的操作
    Precomputed PrecomputedValues
}

2.通过x509标准将得到 的rsa公钥序列化为字符串
func MarshalPKIXPublicKey(pub interface{}) ([]byte, error)


3.将公钥字符串设置到pem格式块中
type Block struct {
    Type    string            // 得自前言的类型（如"RSA PRIVATE KEY"）
    Headers map[string]string // 可选的头项
    Bytes   []byte            // 内容解码后的数据，一般是DER编码的ASN.1结
    	构
}

4.通过pem将设置好的数据进行编码, 并写入磁盘文件
func Encode(out io.Writer, b *Block) error
*/

func GenRsaKey() (prvkey, pubkey []byte) {
	// 生成私钥文件
	privateKey, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		panic(err)
	}
	derStream := x509.MarshalPKCS1PrivateKey(privateKey)
	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: derStream,
	}
	//存到文件里，执行一次，以后从文件中获取私钥和公钥
	//file, _ := os.Create("private.pem")
	//err = pem.Encode(file, block)

	prvkey = pem.EncodeToMemory(block)
	publicKey := &privateKey.PublicKey
	derPkix, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		panic(err)
	}
	block = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: derPkix,
	}

	//存到文件里
	//file, _ = os.Create("public.pem")
	//err = pem.Encode(file, block)

	pubkey = pem.EncodeToMemory(block)
	return
}

//签名
func RsaSignWithSha256(data []byte, keyBytes []byte) []byte {
	h := sha256.New()
	h.Write(data)
	hashed := h.Sum(nil)
	block, _ := pem.Decode(keyBytes)
	if block == nil {
		panic(errors.New("private key error"))
	}
	privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		fmt.Println("ParsePKCS8PrivateKey err", err)
		panic(err)
	}
	//用自己的私钥，加密摘要，生成签名（Signature）
	signature, err := rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, hashed)
	if err != nil {
		fmt.Printf("Error from signing: %s\n", err)
		panic(err)
	}

	return signature
}

//验证
func RsaVerySignWithSha256(data, signData, keyBytes []byte) bool {
	block, _ := pem.Decode(keyBytes)
	if block == nil {
		panic(errors.New("public key error"))
	}
	pubKey, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		panic(err)
	}

	hashed := sha256.Sum256(data)
	err = rsa.VerifyPKCS1v15(pubKey.(*rsa.PublicKey), crypto.SHA256, hashed[:], signData)
	if err != nil {
		panic(err)
	}
	return true
}

// 公钥加密
func RsaEncrypt(data, keyBytes []byte) []byte {
	//解密pem格式的公钥
	block, _ := pem.Decode(keyBytes)
	if block == nil {
		panic(errors.New("public key error"))
	}
	// 解析公钥
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		panic(err)
	}
	// 类型断言
	pub := pubInterface.(*rsa.PublicKey)
	//加密
	ciphertext, err := rsa.EncryptPKCS1v15(rand.Reader, pub, data)
	if err != nil {
		panic(err)
	}
	return ciphertext
}

// 私钥解密
func RsaDecrypt(ciphertext, keyBytes []byte) []byte {
	//获取私钥
	block, _ := pem.Decode(keyBytes)
	if block == nil {
		panic(errors.New("private key error!"))
	}
	//解析PKCS1格式的私钥
	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		panic(err)
	}
	// 解密
	data, err := rsa.DecryptPKCS1v15(rand.Reader, priv, ciphertext)
	if err != nil {
		panic(err)
	}
	return data
}
