package util

import (
	"errors"
	"fmt"
	"github.com/robfig/cron"
	"strings"
	"time"
)

type CronEntity struct {
	WeekEveryday bool `json:"week_every_day"`
	Sunday       bool `json:"sunday"`
	Monday       bool `json:"monday"`
	Tuesday      bool `json:"tuesday"`
	Wednesday    bool `json:"wednesday"`
	Thursday     bool `json:"thursday"`
	Friday       bool `json:"friday"`
	Saturday     bool `json:"saturday"`

	IsPeriodExec bool   `json:"is_period_exec"` //周期触发
	IsOnceExec   bool   `json:"is_once_exec"`   //单次触发
	IsCustomCron bool   `json:"is_custom_cron"` //是否自定义cron表达式
	CustomCron   string `json:"custom_cron"`    //自定义cron表达式
	/*BeginTime string `json:"begin_time"`
	EndTime   string `json:"end_time"`*/
	BeginTime int `json:"begin_time"`
	EndTime   int `json:"end_time"`

	OnceExecTime string `json:"once_exec_time"`

	Interval int `json:"interval"` //间隔 单位分钟
}

func MakeCorn(cronEntity *CronEntity) {
	var build strings.Builder

	/*fmt.Printf("WeekEveryday = %v\n", cronEntity.WeekEveryday)
	fmt.Printf("Sunday = %v\n", cronEntity.Sunday)
	fmt.Printf("Monday = %v\n", cronEntity.Monday)
	fmt.Printf("Tuesday = %v\n", cronEntity.Tuesday)
	fmt.Printf("Wednesday = %v\n", cronEntity.Wednesday)
	fmt.Printf("Thursday = %v\n", cronEntity.Thursday)
	fmt.Printf("Friday = %v\n", cronEntity.Friday)
	fmt.Printf("Saturday = %v\n", cronEntity.Saturday)
	fmt.Printf("IsPeriodExec = %v\n", cronEntity.IsPeriodExec)
	fmt.Printf("IsOnceExec = %v\n", cronEntity.IsOnceExec)
	fmt.Printf("BeginTime = %s\n", cronEntity.BeginTime)
	fmt.Printf("EndTime = %s\n", cronEntity.EndTime)
	fmt.Printf("OnceExecTime = %s\n", cronEntity.OnceExecTime)
	fmt.Printf("Interval = %d\n", cronEntity.Interval)*/

	build.WriteString("0 ")
	if cronEntity.IsPeriodExec == true && cronEntity.IsOnceExec == false && cronEntity.IsCustomCron == false {
		/*
			//处理“16：00”这种字符串 后面如果只有小时的数字  下面这一片代码可以删除
			beginsplit := strings.Split(cronEntity.BeginTime, ":")
			endsplit := strings.Split(cronEntity.EndTime, ":")
			if len(beginsplit) != 2 || len(endsplit) != 2 {
				return
			}
			beginHour := beginsplit[0]
			beginHour = strings.TrimPrefix(beginHour, "0")
			beginMinute := beginsplit[1]
			beginMinute = strings.TrimPrefix(beginMinute, "0")
			endHour := endsplit[0]
			endHour = strings.TrimPrefix(endHour, "0")
			endMinute := endsplit[1]
			endMinute = strings.TrimPrefix(endMinute, "0")*/
		beginHour := cronEntity.BeginTime
		endHour := cronEntity.EndTime
		minute_intervalstr := fmt.Sprintf("0/%d ", cronEntity.Interval) //生成分钟间隔字符串
		build.WriteString(minute_intervalstr)                           //从第零分钟开始间隔5分钟
		hour_str := fmt.Sprintf("%d-%d"+" ", beginHour, endHour)        //生成分钟间隔字符串
		build.WriteString(hour_str)
		build.WriteString("? ") //表示每一天
		build.WriteString("* ") //表示每一月
		if cronEntity.WeekEveryday == true {
			build.WriteString("* ") //表示每一月
			cronspec := build.String()
			fmt.Println(cronspec)
		} else {
			if cronEntity.Sunday == true {
				build.WriteString("1,") //表示每一月
			}
			if cronEntity.Monday == true {
				build.WriteString("2,") //表示每一月
			}
			if cronEntity.Tuesday == true {
				build.WriteString("3,") //表示每一月
			}
			if cronEntity.Wednesday == true {
				build.WriteString("4,") //表示每一月
			}
			if cronEntity.Thursday == true {
				build.WriteString("5,") //表示每一月
			}
			if cronEntity.Friday == true {
				build.WriteString("6,") //表示每一月
			}
			if cronEntity.Saturday == true {
				build.WriteString("7,") //表示每一月
			}
			cronspec := build.String()
			if strings.HasSuffix(cronspec, ",") {
				cronspec = strings.TrimSuffix(cronspec, ",")
				//cronspec = cronspec + " *"
				fmt.Println(cronspec)
				specParser := cron.NewParser(cron.Second | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.DowOptional)
				sched, err := specParser.Parse(cronspec)
				//sched, err := cron.ParseStandard(cronspec)
				if err != nil {
					fmt.Println(err.Error())

				} else {
					if sched != nil {

						var result = make([]time.Time, 10)
						Factorial(time.Now(), sched, 0, result)
						for _, t := range result {
							fmt.Println(t)
						}
					} else {
						errors.New("cron表达式生成失败+")
					}
				}
				return
			}

		}
	}
	if cronEntity.IsPeriodExec == false && cronEntity.IsOnceExec == true && cronEntity.IsCustomCron == false {
		oncetimesplit := strings.Split(cronEntity.OnceExecTime, ":")

		if len(oncetimesplit) != 2 {
			//一次运行时间并不符合格式 “11：00” 返回错误
			return
		}
		hour := oncetimesplit[0]
		hour = strings.TrimPrefix(hour, "0")
		minute := oncetimesplit[1]
		minute = strings.TrimPrefix(minute, "0")

		minute_str := fmt.Sprintf("%s ", minute) //指定分钟
		build.WriteString(minute_str)
		hour_str := fmt.Sprintf("%s ", hour) //指定小时
		build.WriteString(hour_str)

		build.WriteString("? ") //表示每一天
		build.WriteString("* ") //表示每一月
		if cronEntity.WeekEveryday == true {
			build.WriteString("* ") //表示每一月
			cronspec := build.String()
			fmt.Println(cronspec)
		} else {
			if cronEntity.Sunday == true {
				build.WriteString("1,") //表示每一月
			}
			if cronEntity.Monday == true {
				build.WriteString("2,") //表示每一月
			}
			if cronEntity.Tuesday == true {
				build.WriteString("3,") //表示每一月
			}
			if cronEntity.Wednesday == true {
				build.WriteString("4,") //表示每一月
			}
			if cronEntity.Thursday == true {
				build.WriteString("5,") //表示每一月
			}
			if cronEntity.Friday == true {
				build.WriteString("6,") //表示每一月
			}
			if cronEntity.Saturday == true {
				build.WriteString("7,") //表示每一月
			}
			cronspec := build.String()
			if strings.HasSuffix(cronspec, ",") {
				cronspec = strings.TrimSuffix(cronspec, ",")
				//cronspec = cronspec + " *"
				fmt.Println(cronspec)
				specParser := cron.NewParser(cron.Second | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.DowOptional)
				sched, err := specParser.Parse(cronspec)
				//sched, err := cron.ParseStandard(cronspec)
				if err != nil {
					fmt.Println(err.Error())

				} else {
					if sched != nil {

						var result = make([]time.Time, 10)
						Factorial(time.Now(), sched, 0, result)
						for _, t := range result {
							fmt.Println(t)
						}
					} else {
						errors.New("cron表达式生成失败+")
					}
				}
				return
			}

		}
	}
	//处理用户自定义cron的情况
	if cronEntity.IsPeriodExec == false && cronEntity.IsOnceExec == false && cronEntity.IsCustomCron == true {

	}
}

//递归调用自己 生成10个模拟运行时间
func Factorial(time time.Time, schedule cron.Schedule, count int, result []time.Time) (time.Time, cron.Schedule, int, []time.Time) {
	if count < len(result) {
		time = schedule.Next(time)
		result[count] = time
		count++
		Factorial(time, schedule, count, result)
	}
	return time, schedule, count, result
}
