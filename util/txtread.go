package util

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

/*func main() {
	err := HandleText("a.txt")
	if err != nil {
		panic(err)
	}
}*/

func HandleText(fileName string) map[string]string {
	commitInfoMaps := make(map[string]string)
	file, err := os.OpenFile(fileName, os.O_RDWR, 0666)
	if err != nil {
		fmt.Println("Open file error!", err)
		return nil
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		fmt.Println(err)
	}

	var size = stat.Size()
	fmt.Println("file size=", size)

	buf := bufio.NewReader(file)
	for {
		line, err := buf.ReadString('\n')
		line = strings.TrimSpace(line)
		fmt.Println(line)
		splitResults := strings.Split(line, "=")
		if len(splitResults) == 2 {
			commitInfoMaps[splitResults[0]] = splitResults[1]
		}

		if err != nil {
			if err == io.EOF {
				fmt.Println("File read ok!")
				break
			} else {
				fmt.Println("Read file error!", err)
				return nil
			}
		}
	}
	return commitInfoMaps
}
