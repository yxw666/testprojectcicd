package containerregistry

import (
	"fmt"
	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/google/go-containerregistry/pkg/name"
	"github.com/google/go-containerregistry/pkg/v1/remote"
)

func maintestcontainer() {
	//ref, err := name.ParseReference("gcr.io/google-containers/pause")
	ref, err := name.ParseReference("172.20.140.158:8081/test-docker/v2/test-image/animal:v2.0")
	//ref, err := name.ParseReference("myharbor.com:80/harbor/projects/3/repositories")
	if err != nil {
		panic(err)
	}

	get, err := remote.Get(ref, remote.WithAuthFromKeychain(authn.DefaultKeychain))
	img, err := remote.Image(ref, remote.WithAuth(authn.Anonymous))
	if err != nil {
		panic(err)
	}
	layers, err := img.Layers()
	for i := range layers {
		fmt.Println(i)
	}

	fmt.Println(get)
	fmt.Println(img)
	fmt.Println(img.Size())

	// do stuff with img
}
