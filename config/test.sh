#!/bin/bash
IMAGE_TAG="202201161249"
Build_Branch="test1249"

if [ -f "./config/commitinfo.txt" ];then
  echo "file exist"
  sed -i 's/commitID=/commitID='${IMAGE_TAG}'oldvalue1/g' ./config/commitinfo.txt
  sed -i 's/oldvalue1.*$//g'  ./config/commitinfo.txt
  sed -i 's/commitBranch=/commitBranch='${Build_Branch}'oldvalue2/g' ./config/commitinfo.txt
  sed -i 's/oldvalue2.*$//g'  ./config/commitinfo.txt
  DATE=`date '+%Y%m%d-%H%M%S'`
  sed -i 's/commitTime=/commitTime='${DATE}'oldvalue3/g' ./config/commitinfo.txt
  sed -i 's/oldvalue3.*$//g'  ./config/commitinfo.txt
else
  echo "file not exist, will create init file "
  #sudo cat>>./config/commitinfo.txt<<EOF
  #commitID=ceshiyixia
  #commitTime=2022-11-11
  #commitBranch=develpo
  #EOF
  echo "commitID=ceshiyixia"> ./config/commitinfo.txt
  chmod 666 ./config/commitinfo.txt
  echo "commitBranch=develpo" >> ./config/commitinfo.txt
  echo "commitTime=$(date +"%Y-%m-%d %H:%M:%S")" >> ./config/commitinfo.txt
  

fi


