package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

// NewProxy takes target host and creates a reverse proxy
func NewProxy(targetHost string) (*httputil.ReverseProxy, error) {
	url, err := url.Parse(targetHost)
	if err != nil {
		return nil, err
	}
	proxy := httputil.NewSingleHostReverseProxy(url)
	originalDirector := proxy.Director

	proxy.Director = func(req *http.Request) {
		originalDirector(req)
		modifyRequest(req)
	}
	proxy.ModifyResponse = modifyResponse()
	//proxy.ErrorHandler = errorHandler()
	return proxy, nil
}
func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}
func modifyRequest(req *http.Request) {
	req.Header.Set("Authorization", "Basic "+basicAuth("admin", "admin123"))
}
func errorHandler() func(http.ResponseWriter, *http.Request, error) {
	return func(w http.ResponseWriter, req *http.Request, err error) {
		fmt.Printf("Got error while modifying response: %v \n", err)
		return
	}
}
func modifyResponse() func(*http.Response) error {
	return func(resp *http.Response) error {

		resp.Header.Set("X-Proxy", "Magical")
		return nil
		//return errors.New("response body is invalid")
	}
}

// ProxyRequestHandler handles the http request using proxy
func ProxyRequestHandler(proxy *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// 认证鉴权
		// 业务处理
		fmt.Println("test111111")
		// 代理转发
		w.WriteHeader(404)
		proxy.ServeHTTP(w, r)
	}
}
func main() {
	// initialize a reverse proxy and pass the actual backend server url here
	proxy, err := NewProxy("http://192.168.26.1:9999/")
	if err != nil {
		panic(err)
	}
	// handle all requests to your server using the proxy
	http.HandleFunc("/", ProxyRequestHandler(proxy))
	log.Fatal(http.ListenAndServe(":9990", nil))
}

/*func main() {
	s := []string{"12/16", "12/17", "12/18", "12/19", "12/20", "12/21", "12/22", "12/23", "12/24", "12/25", "12/26", "12/27", "12/28", "12/29", "12/30", "12/31", "01/01", "01/02", "01/03", "01/04", "01/05", "01/06", "01/07", "01/08", "01/09", "01/10", "01/11", "01/12", "01/13", "01/14"}
	index := sort.SearchStrings(s, "01/01")
	fmt.Println(index)

	commitID := "cat /opt/test.txt | grep 'commitID=' | sed -i 's/commitID=/commitID=adminoldvalue1/g' /opt/test.txt  | sed -i 's/oldvalue1.*$//g'  /opt/test.txt"
	commitTime := "cat /opt/test.xml | grep 'commitTime=' | sed -i 's/commitTime=/commitTime=adminoldvalue2/g' /opt/test.xml  | sed -i 's/oldvalue2.*$//g'  /opt/test.xml"
	commitor := "cat /opt/test.xml | grep 'commitor=' | sed -i 's/commitor=/commitor=adminoldvalue3/g' /opt/test.xml  | sed -i 's/oldvalue3.*$//g'  /opt/test.xml"
	             cat /opt/test.xml | grep 'commitor=' | sed -i 's/commitor=/commitor=adminoldcommitor/g' /opt/test.xml  | sed -i 's/oldcommitor.*$//g'  /opt/test.xml
	               cat /opt/test.xml | grep 'commitTime=' | sed -i 's/commitTime=/commitTime=adminoldcommitTime/g' /opt/test.xml  | sed -i 's/oldcommitTime.*$//g'  /opt/test.xml

}*/
